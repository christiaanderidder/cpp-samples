#include "Main.h";

using namespace std; // Import the std namespace so we don't have to type std:: in front of everything from std.

// Entry point
int main()
{
	dynamicAllocation();
	simpleStruct();
	return 0;
}

void dynamicAllocation()
{

	// We will use this pointer to point to the dynamically allocated memory
	int * p_dynamic_array;

	// Ask the user to define the amount of memory that should be allocated
	int size_to_allocate;
	std::cout << "How many numbers would you like to type? ";

	// Store the input in our variable
	std::cin >> size_to_allocate;

	// Instead of using plain "new", we "new(std::nothrow)", this will make sure an exception will
	// not be thrown when the memory could not be allocated, instead a null pointer will be returned.
	p_dynamic_array = new(std::nothrow) int[size_to_allocate];

	// Check if the memory was allocated succesfully.
	// NULL is actually an alias for 0, so a null pointer will point to the memory located at 0.
	// It is also possible to use nullptr which has the same meaning, but with it's own type nullptr_t
	// The advantage of this custom type is that it is possible to create a method to specifically accept
	// a parameter of type nullptr_t, where NULL could match multiple types (e.g. int*, double*)
	if (p_dynamic_array == NULL)
	{
		std::cout << "Error: memory could not be allocated";
	}
	else
	{
		// Ask the user to put a value in every allocated space.
		for (int i = 0; i < size_to_allocate; i++)
		{
			std::cout << "Enter number: ";
			std::cin >> p_dynamic_array[i]; // Put the input in value at the address p_dynamic_array + (i * size of an integer)
		}

		// Print the input
		std::cout << "You have entered: ";
		for (int i = 0; i < size_to_allocate; i++)
		{
			std::cout << p_dynamic_array[i] << ", ";
		}

		// Free the dynamically allocated memory.
		delete[] p_dynamic_array;
	}
}

void simpleStruct()
{
	Person person;

	std::cout << "What is your name?";
	std::cin >> person.name;
	std::cout << "How old are you?";
	std::cin >> person.age;
	std::cout << "Hello " << person.name << " (" << person.age << ")";
}