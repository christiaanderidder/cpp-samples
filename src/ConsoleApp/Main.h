#pragma once

// Library files
#include <iostream>; // Input and output streams, so we can get cIN and cOUT.
#include <new>; // Dyanamic memory allocation, so we can use new.
#include <string>;

// Custom headers
#include "Person.h";

// Definitions for Main.cpp
void dynamicAllocation();
void simpleStruct();