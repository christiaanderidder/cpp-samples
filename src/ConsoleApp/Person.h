#pragma once

#include <string>; // C++ strings as opposed to using char * / char[] (c-strings)

// An example of a simple c++ struct.
struct Person
{
	std::string name;
	int age;
};